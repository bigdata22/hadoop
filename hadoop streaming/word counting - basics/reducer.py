#!/usr/bin/env python
import sys

if __name__ == '__main__':

    #sys.stderr.write('reporter:status:{}\n'.format('reduce started'))
    currentkey = None
    total = 0
    for line in sys.stdin.readlines():
        key, value = line.split('\t')
        if key == currentkey:
            total += int(value)
        else:
            if currentkey is not None:
                sys.stdout.write('{}{}{}\n'.format(currentkey, '\t', total))
            total = int(value)
            currentkey = key

