#!/usr/bin/env python
import sys
import os

if __name__ == '__main__':

    sys.stderr.write('reporter:status:{}\n'.format('map started'))
    for line in sys.stdin.readlines():
        for word in line.split():
            if word is not None:
                sys.stdout.write('{}{}{}\n'.format(word, '\t', 1))
            