#!/usr/bin/env python

import os
import sys
from itertools import groupby
from operator import itemgetter

SEPERATORE = '\t'


class streaming(object):
    
    def __init__(self, infile=sys.stdin ,seperator=SEPERATORE):
        self.sep = seperator
        self.infile = infile

    def status(self, message):
        sys.stderr.write('reporter:status:{}\n'.format(message))

    def counter(self, counter, amount=1, group='Hadoop Streaming'):
        message = "reporter:counter:{}{}{}\n".format(group, counter, amount)
        sys.stderr.write(message)

    def emit(self, key, value):
        sys.stdout.write("{}{}{}\n".format(key, self.sep ,value))

    def read(self):
        for line in self.infile:
            yield line.rstrip()

    def __iter__(self):
        for line in self.read():
            yield line

    