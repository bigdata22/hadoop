#!/usr/bin/env python
import sys
SEPERATORE = '\t'

class Streaming(object):
    
    def __init__(self, infile=sys.stdin ,seperator=SEPERATORE):
        self.sep = seperator
        self.infile = infile

    def status(self, message):
        sys.stderr.write('reporter:status:{}\n'.format(message))

    def counter(self, counter, amount=1, group='Hadoop Streaming'):
        message = "reporter:counter:{}{}{}\n".format(group, counter, amount)
        sys.stderr.write(message)

    def emit(self, key, value):
        sys.stdout.write("{}{}{}\n".format(key, self.sep ,value))

    def read(self):
        for line in self.infile.readlines():
            yield line

    def __iter__(self):
        for line in self.read():
            yield line

class Mapper(Streaming):
    def map(self):
        raise NotImplementedError("Mappers must impelement a map method")

class Reducer(Streaming):
    def reduce(self):
        raise NotImplementedError("Reducers must impelement a reduce method")


