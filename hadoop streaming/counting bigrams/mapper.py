#!/usr/bin/env python
import string
import sys

from nltk import wordpunct_tokenize
from nltk import bigrams
from nltk.corpus import stopwords

from framework import Mapper

class BigramMapper(Mapper):
    
    def __init__(self, infile=sys.stdin, seperator='\t'):
        super().__init__(infile=infile, seperator=seperator)

        self.stopwords = stopwords.words("english")
        self.punctuation = string.punctuation

    def normalize(self, token):
        return token.lower()

    def exclude(self, token):
        return token in self.stopwords or token in self.punctuation

    def tokenize(self, value):
        for token in wordpunct_tokenize(value):
            token = self.normalize(token)
            if not self.exclude(token):
                yield token

    def map(self):
        for line in self:
            for bigram in bigrams(self.tokenize(line.rstrip())):
                self.emit(bigram, 1)

if __name__ == '__main__':
    mapper = BigramMapper()
    mapper.map()

            