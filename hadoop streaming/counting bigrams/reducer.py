#!/usr/bin/env python
from os import sep
import sys
from framework import Reducer
from itertools import groupby

class SumReducer(Reducer):
    
    def __init__(self, infile=sys.stdin, seperator='\t'):
        super().__init__(infile=infile, seperator=seperator)


    def reduce(self):
        currentkey = None
        total = 0
        for line in self:
            key, value = line.split('\t')
            
            if key == currentkey:
                total += int(value)
            else:
                if currentkey is not None:
                    self.emit(currentkey, total)
                
                total = int(value)
                currentkey = key


if __name__ == '__main__':
    reducer = SumReducer()
    reducer.reduce()