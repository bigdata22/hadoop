chmod +x mapper.py
chmod +x reducer.py


cat file.txt | python3 mapper.py |sort| python3 reducer.py


hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming*.jar 
-input flights.tsv 
-output average_delay 
-mapper /home/bigdata/source/bts/mapper.py 
-reducer /home/bigdata/source/bts/reducer.py
-file /home/bigdata/source/bts/framework.py
