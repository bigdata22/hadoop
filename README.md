**Installing Hadoop On Ubuntu 20.x**


_**Pre Requestes :**_
- **Install OpenJDK on Ubuntu**
1. sudo apt update
2. sudo apt install openjdk-8-jdk -y (Current Version of JDK)
3. java -version; javac -version

- **Set Up a Non-Root User for Hadoop Environment**
1. Install OpenSSH on Ubuntu
    - sudo apt install openssh-server openssh-client -y
2. Create Hadoop User
    - sudo adduser hadoop
    - su - hdoop
3. Enable Passwordless SSH for Hadoop User
    - ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
    - cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
    - chmod 0600 ~/.ssh/authorized_keys
    - ssh localhost (Verify everything is set up correctly by using the hdoop user to SSH to localhost)


_**Download and Install Hadoop on Ubuntu**_
1. wget https://downloads.apache.org/hadoop/common/hadoop-3.2.2/hadoop-3.2.2.tar.gz
2. tar xzf hadoop-3.2.2.tar.gz

**Single Node Hadoop Deployment (Pseudo-Distributed Mode)**
1. Configure Hadoop Environment Variables (bashrc)
    - sudo nano .bashrc  (.bashrc is in root of the user (~) for example **hadoop** user which was created in pre requests)
    - add some env variable to end of the .bashrc

        #Hadoop Related Options
        - export HADOOP_HOME=/home/hadoop/hadoop-3.2.2
        - export HADOOP_INSTALL=$HADOOP_HOME
        - export HADOOP_MAPRED_HOME=$HADOOP_HOME
        - export HADOOP_COMMON_HOME=$HADOOP_HOME
        - export HADOOP_HDFS_HOME=$HADOOP_HOME
        - export YARN_HOME=$HADOOP_HOME
        - export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
        - export PATH=$PATH:$HADOOP_HOME/sbin:$HADOOP_HOME/bin
        - export HADOOP_OPTS="-Djava.library.path=$HADOOP_HOME/lib/nativ"

        #JAVA Related Options
        - export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64
        - export PATH=$PATH:/usr/lib/jvm/java-1.8.0-openjdk-amd64/bin
        - export CLASSPATH=$CLASSPATH:/usr/lib/jvm/java-1.8.0-openjdk-amd64/lib/tools.jar
        - export HADOOP_CLASSPATH=$(find $HADOOP_HOME -name '*.jar' | xargs echo | tr ' >
        - export HADOOP_CLASSPATH=$HADOOP_CLASSPATH:${JAVA_HOME}/lib/tools.jar

    - source ~/.bashrc (It is vital to apply the changes to the current running environment)

2. Edit hadoop-env.sh File
    - sudo nano $HADOOP_HOME/etc/hadoop/hadoop-env.sh
    - export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 (Add this to hadoop-env.sh)

3. Edit core-site.xml File
    - sudo nano $HADOOP_HOME/etc/hadoop/core-site.xml
    - Create directory in /home/hadoop/tmpdata
    - Add the following configuration to override the default values for the temporary directory and add your HDFS URL
           to replace the default local file system setting:

```
        <configuration>
            <property>
                <name>hadoop.tmp.dir</name>
                <value>/home/hadoop/tmpdata</value>
            </property>
            <property>
                <name>fs.default.name</name>
                <value>hdfs://127.0.0.1:9000</value>
            </property>
        </configuration>
```


4. Edit hdfs-site.xml File
    - sudo nano $HADOOP_HOME/etc/hadoop/hdfs-site.xml
    - Create directory in /home/hadoop/dfsdata/namenode
    - Create directory in /home/hadoop/dfsdata/datanode
    - Add the following configuration to the file and, if needed, adjust the NameNode and DataNode directories to your          custom locations:

```
        <configuration>
            <property>
                <name>dfs.data.dir</name>
                <value>/home/hadoop/dfsdata/namenode</value>
            </property>
            <property>
                <name>dfs.data.dir</name>
                <value>/home/hadoop/dfsdata/datanode</value>
            </property>
            <property>
                <name>dfs.replication</name>
                <value>1</value>
            </property>
        </configuration>
```
5. Edit mapred-site.xml File
    - sudo nano $HADOOP_HOME/etc/hadoop/mapred-site.xml
    - Add the following configuration to change the default MapReduce framework name value to yarn:

```
        <configuration> 
            <property> 
                <name>mapreduce.framework.name</name> 
                <value>yarn</value> 
            </property> 
        </configuration>
```
6. Edit yarn-site.xml File
    - The yarn-site.xml file is used to define settings relevant to YARN. It contains configurations for the Node Manager, Resource Manager, Containers, and Application Master
    - sudo nano $HADOOP_HOME/etc/hadoop/yarn-site.xml
    - Append the following configuration to the file:

```
        <configuration>
            <property>
                <name>yarn.nodemanager.aux-services</name>
                <value>mapreduce_shuffle</value>
            </property>
            <property>
                <name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
                <value>org.apache.hadoop.mapred.ShuffleHandler</value>
            </property>
            <property>
                <name>yarn.resourcemanager.hostname</name>
                <value>127.0.0.1</value>
            </property>
            <property>
                <name>yarn.acl.enable</name>
                <value>0</value>
            </property>
            <property>
                <name>yarn.nodemanager.env-whitelist</name>   
                <value>JAVA_HOME,HADOOP_COMMON_HOME,HADOOP_HDFS_HOME,HADOOP_CONF_DIR,CLASSPATH_PERPEND_DISTCACHE,HADOOP_YARN_HOME,HADOOP_MAPRED_HOME</value>
            </property>
        </configuration>
```

**Format HDFS NameNode**
- hdfs namenode -format

**Start Hadoop Cluster**
- Navigate to the hadoop-3.2.2/sbin directory and execute the following commands to start the NameNode and DataNode
- ./start-dfs.sh
- Once the namenode, datanodes, and secondary namenode are up and running, start the YARN resource and nodemanagers by typing
- ./start-yarn.sh
- Type this simple command to check if all the daemons are active and running as Java processes:
- jps


**Access Hadoop UI from Browser**
- The default port number 9870 gives you access to the Hadoop NameNode UI (http://localhost:9870)
- The default port 9864 is used to access individual DataNodes directly from your browser (http://localhost:9864)
- The YARN Resource Manager is accessible on port 8088 (http://localhost:8088)














